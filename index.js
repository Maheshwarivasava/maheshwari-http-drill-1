const http = require("http");
const path = require("path");
const fs = require("fs");
const uuid = require("uuid");
const htmlfile = "./index.html";
const jsonfile = "./data.json";
const PORT = process.env.port || 8000;
function generate_random_uuid() {
  const uuid_string = uuid.v4();

  return uuid_string;
}

// const file_path = path.join(__dirname, file);

const server = http.createServer((request, response) => {
  const statusCode = request.url.split("/")[2];
  const delayInSeconds = request.url.split("/")[2];
  if (request.url === "/html" && request.method === "GET") {
    fs.readFile(htmlfile, "utf-8", (error, data) => {
      if (error) {
        console.log(error);
        response.writeHead(500, { "Content-Type": "application/json" });
        const errorMessage = {
          message: "Facing problem in reading html file",
        };
        response.write(JSON.stringify(errorMessage));
        response.end();
      } else {
        response.writeHead(200, { "Content-Type": "text/html" });
        response.write(data);
        response.end();
      }
    });
  } else if (request.url === "/json" && request.method === "GET") {
    if (
      fs.readFile(jsonfile, "utf-8", (error, data) => {
        if (error) {
          console.log(error);
          response.writeHead(500, { "Content-Type": "application/json" });
          const errorMessage = {
            message: "Facing problem in reading json file",
          };
          response.write(JSON.stringify(errorMessage));
          response.end();
        } else {
          response.writeHead(200, { "Content-Type": "application/json" });
          response.write(data);
          response.end();
        }
      })
    );
  } else if (request.url === "/uuid" && request.method === "GET") {
    response.writeHead(200, { "Content-Type": "application/json" });

    response.write(JSON.stringify(`uuid : ${generate_random_uuid()}`));

    response.end();
  } else if (
    request.url === `/status/${statusCode}` &&request.method === "GET") {
    try {
      response.writeHead(statusCode, { "Content-Type": "application/json" });
      const statusMessage = {
        status: `${statusCode} ${http.STATUS_CODES[statusCode]}`,
      };
      response.write(JSON.stringify(statusMessage));
      response.end();
    } catch {
      response.writeHead(400, { "Content-Type": "application/json" });
      const errorMessage = {
        message: "Invalid status code",
      };
      response.write(JSON.stringify(errorMessage));
      response.end();
    }
  } else if ( request.url === `/delay/${delayInSeconds}` &&request.method === "GET" ) {
    const seconds = Number(delayInSeconds);
    if (isNaN(seconds)) {
        response.writeHead(400, { 'Content-Type': 'application/json' });
        const errorMessage = {
            message: "Seconds should be a number"
        }
        response.write(JSON.stringify(errorMessage));
        response.end();
    }else {
        setTimeout(() => {
            response.writeHead(200, { 'Content-Type': 'application/json' });
            const delayTimes = {
                'delayInseconds': seconds
            };
            response.write(JSON.stringify(delayTimes));
            response.end();
        }, seconds * 1000);
    }


  } else {
    response.writeHead(404, { "Content-Type": "application/json" });
    const errorMessage = {
      message: "This url doesn't exist",
    };
    response.write(JSON.stringify(errorMessage));
    response.end();
  }
});

server.listen(PORT, () => {
  console.log(`Server is listening on port ${PORT}`);
});
